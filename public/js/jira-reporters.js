var urlParams = new URLSearchParams(window.location.search);
var projectId = urlParams.get("projectId");

// Parameterise the project identifier so that the page dynamically retrieves the
// issues associated with the current project only.
AP.request('/rest/api/3/search?jql=project=' + projectId + '+order+by+reporter&fields=reporter', {
    success: function(responseText){
        var reporterTable = document.getElementById("reporters");
        var data = JSON.parse(responseText);

		var reporterName = null;
		var issueCount = 0;
		var nameCell = null;
		var countCell = null;
		
		for (var i=0; i < data.issues.length; i++) {
			var issue = data.issues[i];
			if (issue.fields.reporter.displayName !== reporterName) {
				reporterName = issue.fields.reporter.displayName;
				issueCount = 0;
				
				var newRow = reporterTable.insertRow(-1);
				var nameCell = newRow.insertCell(0);
				var	countCell = newRow.insertCell(1);

				nameCell.innerHTML = reporterName;
			}
			
			countCell.innerHTML = ++issueCount;
		}
		
		reporterTable.deleteRow(1);
    }
});